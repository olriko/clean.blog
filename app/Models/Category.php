<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name',
        'description',

        'navbar_display',
        'navbar_name',

        'order'
    ];

    public function articles()
    {
        return $this->hasMany(Article::class);
    }
}
