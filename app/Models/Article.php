<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Storage;

class Article extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'category_id',
        'user_id',

        'name',
        'description',
        'image',
        'content',
        'views',
        'published_at',
        'published'
    ];

    protected $dates = [
      'published_at'
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function getImageAttribute($value)
    {
        if ($value) {
            return  Storage::url('article/image/' . $value);
        } else {
            return null;
        }
    }

    public function setImageAttribute($value)
    {
        if ($value) {
            $name = str_slug($this->name) . '_' . uniqid() . '.' . $value->getClientOriginalExtension();
            $storage = Storage::disk('public')->putFileAs('article/image',  $value, $name);
            if ($storage) {
                $this->attributes['image'] = $name;
            }
        }
    }

    public function scopeIsPublished($query)
    {
        return $query->where('published', true)->where('published_at', '<', Carbon::now());
    }
}
