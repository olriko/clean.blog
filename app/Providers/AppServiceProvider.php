<?php

namespace App\Providers;

use App\Models\Category;
use Illuminate\Support\ServiceProvider;
use Schema;
use Blade;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        Blade::directive('active', function ($expression) {
            return "<?php echo active_url($expression); ?>";
        });

        View::composer(['partials.header', 'partials.footer'], function($view) {
           $view->with('categories', Category::where('navbar_display', true)->orderBy('order')->get()->take(6));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
