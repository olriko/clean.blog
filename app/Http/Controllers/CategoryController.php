<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function show( $name, $id)
    {
        $category = Category::findOrFail($id);

        return view('category', [
            'category' => $category,
            'articles' => $category->articles()->IsPublished()->orderBy('published_at', 'DESC')->paginate(9)
        ]);
    }
}
