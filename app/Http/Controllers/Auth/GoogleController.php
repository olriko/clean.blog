<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Socialize;
use Settings;
use Auth;

class GoogleController extends Controller
{
    public function __construct()
    {
        if (Settings::get('auth_google_id') && Settings::get('auth_google_secret')) {
            config([
                'services.google.client_id' => Settings::get('auth_google_id'),
                'services.google.client_secret' => Settings::get('auth_google_secret'),
                'services.google.redirect' => route('login.google.handle')
            ]);
        }
    }

    public function redirect()
    {
        return Socialize::with('google')->redirect();
    }

    public function handle()
    {
        $provider = Socialize::with('google')->user();

        $user = User::where('email', $provider->email)->first();

        if (!$user) {
            $user = new User();

            $user->email = $provider->email;
            $user->name =  $provider->name;
            $user->avatar =  $provider->avatar_original . '?sz=180';

            $user->save();

            if ($user->id == 1) {
                $user->admin = true;
                $user->save();
            }
        }

        Auth::login($user);

        return redirect()->route('home');
    }
}
