<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function show($title, $id)
    {
        $article = Article::findOrFail($id);

        $article->increment('views');

        return view('article.show', [
           'article' => $article
        ]);
    }

    public function comment(Request $request, $id)
    {
        $article = Article::findOrFail($id);


        $this->validate($request, [
            'comment' => 'required'
        ]);

        $article->comments()->create([
            'content' => $request->get('comment'),
            'user_id' => auth()->user()->id
        ]);

        return redirect()->back();
    }
}
