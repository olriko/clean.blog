<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ArticleRequest;
use App\Models\Article;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Storage;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("admin.article.index", ['articles' => Article::paginate(20)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.article.create", ['article' => new Article()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleRequest $request)
    {
        $article = new Article();

        $article->name = $request->get('name');
        $article->category_id = $request->get('category_id');
        $article->user_id = $request->get('user_id');
        $article->description = $request->get('description');
        $article->image = $request->file('image');
        $article->content = $request->get('content');

        $article->published_at = $request->get('submit') == 'publish' ? Carbon::now() : null;

        $article->published = $request->get('submit') == 'publish' ? true : false;

        $article->save();

        if ($request->get('submit') == 'save') {
            return redirect()->route('admin.article.edit', ['article' => $article]);
        }
        return redirect()->route('admin.article.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Article::findOrFail($id);

        return view('admin.article.edit', ['article' => $article]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ArticleRequest $request, $id)
    {
        $article = Article::findOrFail($id);

        $article->name = $request->get('name');
        $article->category_id = $request->get('category_id');
        $article->user_id = $request->get('user_id');
        $article->description = $request->get('description');
        $article->image = $request->file('image');
        $article->content = $request->get('content');

        switch ($request->get('submit')) {
            case 'publish':
                $article->published_at = Carbon::now();
                if (!$article->image) {
                    $this->validate($request, [
                        'image' => 'required|image'
                    ]);
                }
                $article->published = true;
                break;
            case 'unpublish':
                $article->published = false;
                break;
        }

        $article->save();

        if ($request->get('submit') == 'save') {
            return redirect()->route('admin.article.edit', ['article' => $article]);
        }

        return redirect()->route('admin.article.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article = Article::findOrFail($id);

        $article->delete();

        return redirect()->route('admin.article.index');
    }
}
