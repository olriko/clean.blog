<?php

namespace App\Http\Controllers\Admin;

use App\Models\Article;
use App\Models\Comment;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Storage;

class HomeController extends Controller
{
    public function index()
    {
        $users = User::get();

        $articles = Article::get();

        $articles_by_comments = Article::with('comments')->get()->sortByDesc(function ($query) {
            return $query->comments->count();
        })->take(3);

        $articles_by_views = Article::orderBy('views','DESC')->get()->take(3);

        $comments = Comment::get();

        return view('admin.home', [
            'articles' => $articles,
            'users' => $users,
            'comments' => $comments,
            'articles_by_comments' => $articles_by_comments,
            'articles_by_views' => $articles_by_views
        ]);
    }

    public function storeSettings()
    {
        return redirect()->route('admin.settings');

    }

    public function upload(Request $request)
    {
        $this->validate($request, [
            'image' => 'required|image'
        ]);

        $image = $request->file('image');
        $name = uniqid() . rand(10, 99) . '_' . $image->getClientOriginalName();
        Storage::disk('public')->putFileAs('/image', $image, $name);

        return response()->json([
            'success' => true,
            'url' => '/image/' . $name
        ]);
    }
}
