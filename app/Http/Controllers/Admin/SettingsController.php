<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SettingsRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Settings;
use Storage;


class SettingsController extends Controller
{
    public function index()
    {
        return view('admin.settings.index');
    }

    public function store(SettingsRequest $request)
    {
        Settings::set('app_name', $request->get('app_name'));
        Settings::set('footer_content', $request->get('footer_content'));
        Settings::set('ga', $request->get('ga'));

        Settings::set('social_instagram', $request->get('social_instagram'));
        Settings::set('social_twitter', $request->get('social_twitter'));
        Settings::set('social_facebook', $request->get('social_facebook'));
        Settings::set('social_youtube', $request->get('social_youtube'));
        Settings::set('social_linkedIn', $request->get('social_linkedIn'));

        Settings::set('auth_google_id', $request->get('auth_google_id'));
        Settings::set('auth_google_secret', $request->get('auth_google_secret'));

        if ($request->file('favicon')) {
            Storage::disk('public')->putFileAs('settings', $request->file('favicon'), 'favicon.png');
        }

        if ($request->file('logo')) {
            Storage::disk('public')->putFileAs('settings', $request->file('logo'), 'logo.png');
        }

        return redirect()->route('admin.settings.index');
    }
}
