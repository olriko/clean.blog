<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::IsPublished()->orderBy('published_at', 'DESC')->paginate(9);

        return view('home', [
            'articles' => $articles
        ]);
    }

    public function settings(Request $request)
    {
        $user = auth()->user();

        if ($request->isMethod('POST')) {
            $this->validate($request, [
                'name' => 'required|max:25',
                'password' => 'nullable|min:6|confirmed',
            ]);

            $user->name = $request->get('name');
            if ($request->get('password')) {
                $user->password = bcrypt($request->get('password'));
            }
            $user->save();

            request()->session()->flash('success', 'Settings saved !');

            return redirect()->route('settings');
        }

        return view('settings');
    }

    public function search(Request $request)
    {
        if (!$request->get('query')) {
            return redirect()->route('home');
        }

        $articles = Article::IsPublished()
            ->orderBy('published_at', 'DESC')
            ->where('name' ,'LIKE', '%' . $request->get('query') .  '%')
            ->IsPublished()
            ->paginate(9);

        return view('search', [
            'articles' => $articles,
            'query' => $request->get('query')
        ]);
    }
}
