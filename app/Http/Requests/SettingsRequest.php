<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SettingsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //dd($this->request->all());
        return [
            'app_name' => 'required|max:20',
            'footer_content' => '',
            'favicon' => 'nullable|image|dimensions:width=32,height=32|mimes:png',
            'logo' => 'nullable|image|dimensions:max_height=70|mimes:png',

            'social_youtube' => 'nullable|url',
            'social_twitter' => 'nullable|url',
            'social_facebook' => 'nullable|url',
            'social_instagram' => 'nullable|url',
            'social_linkedIn' => 'nullable|url',

            'auth_google_id' => 'nullable|required_with:auth_google_secret',
            'auth_google_secret' => 'nullable|required_with:auth_google_id'
        ];
    }
}
