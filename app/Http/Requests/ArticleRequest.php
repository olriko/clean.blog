<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->request->get('submit')){
            case 'save':
                return  [
                    'category_id' => 'nullable|exists:categories,id',
                    'user_id' => 'exists:users,id',
                    'submit' => 'required|in:save,publish,unpublish',

                    'name' => 'required|string',

                    'description' => 'nullable',
                    'image' => 'image|nullable|dimensions:width=750,height=300',
                    'content ' => 'nullable',
                ];
            case 'unpublish':
                return  [
                    'category_id' => 'nullable|exists:categories,id',
                    'user_id' => 'exists:users,id',
                    'submit' => 'required|in:save,publish,unpublish',

                    'name' => 'required|string',

                    'description' => 'nullable',
                    'image' => 'image|nullable|dimensions:width=750,height=300',
                    'content ' => 'nullable',
                ];
            case 'publish':
                return [
                    'category_id' => 'nullable|exists:categories,id',
                    'user_id' => 'required|exists:users,id',
                    'submit' => 'required|in:save,publish,unpublish',

                    'name' => 'required|string',

                    'description' => 'required',
                    'image' => 'image|dimensions:width=750,height=300',
                    'content ' => '',
                ];
            default:
                return abort(500);
        }

    }
}
