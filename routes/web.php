<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();



Route::get('/', 'HomeController@index')->name('home');

Route::get('/search', 'HomeController@search')->name('search');

Route::get('/{title}/{id}', 'ArticleController@show')->name('article.show')->where('id', '\d+');

Route::get('/category/{name}/{id}', 'CategoryController@show')->name('category.show')->where('id', '\d+');


Route::get('settings', 'HomeController@settings')->middleware('auth')->name('settings');
Route::post('settings', 'HomeController@settings')->middleware('auth');




Route::post('/comment/{id}', 'ArticleController@comment')
    ->name('comment.store')
    ->where('id', '\d+')
    ->middleware('auth');

//Google auth
Route::get('/login/google/redirect', 'Auth\GoogleController@redirect')->name('login.google.redirect');
Route::get('/login/google/handle', 'Auth\GoogleController@handle')->name('login.google.handle');


Route::group(['middleware' => 'admin', 'namespace' => 'Admin', 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::post('image/upload', 'HomeController@upload')->name('image.upload');

    Route::get('/settings', 'SettingsController@index')->name('settings.index');
    Route::post('/settings', 'SettingsController@store')->name('settings.store');

    Route::resource('user', 'UserController');
    Route::resource('article', 'ArticleController');
    Route::resource('category', 'CategoryController');
});
