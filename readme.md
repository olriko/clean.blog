## Hello ! Thanks for purchasing  ! ##

This project is based on **Laravel 5.4**. It's easy to customize if you have small knowledge with this [framework](https://laravel.com/docs/5.4).

**Server Requirements:**

 - PHP >= 5.6.4
 - OpenSSL PHP Extension
 - PDO PHP Extension
 - Mbstring PHP Extension
 - Tokenizer PHP Extension
 - XML PHP Extension

####First Setup: ####

**Important: You may renamed the .env.example file to .env and you have to fill all the file like database fields.**

**Database:**

If you have an ssh access, you can run `php artisan migrate` to setup database and tables.
If you don't have ssh access, you have database config -> `database/db.sql`

#### Customize color ####

**To customize, you need nodejs and npm to compile scss in css.** 

Once you have nodejs and npm, You may install the dependencies it references by running: `npm install`

All scss are in` /resources/assets/sass` folder. If you want just customize colors, I advise you to edit ` _variables.scss` file. 

Once you have edited files, you may run this command : `npm run production`

For more informations about [sass lang](http://sass-lang.com/guide) and [laravel compilation](https://laravel.com/docs/5.4/frontend) . 

#### Customize words ####

You can edit `/resources/lang/main` to modify commons words


