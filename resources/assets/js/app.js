/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

$.fn.datetimepicker = require('eonasdan-bootstrap-datetimepicker');


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('auth', require('./components/Auth.vue'));

const app = new Vue({
    el: '#app'
});

$(document).ready(function () {
    $("#dtp").datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss'
    });

    $('#content').trumbowyg({
        removeformatPasted: true,
        btnsDef: {
            image: {
                dropdown: ['insertImage', 'upload'],
                ico: 'insertImage'
            }
        },
        btns: [
            'viewHTML',
            '|', 'formatting',
            '|', 'btnGrp-semantic',
            '|', 'link',
            '|', 'image',
            '|', 'btnGrp-justify',
            '|', 'btnGrp-lists',
            '|', 'horizontalRule'
        ],
        plugins: {
            upload: {
                serverPath: '/admin/image/upload',
                fileFieldName: 'image',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                urlPropertyName: 'url'
            }
        }
    });
});
