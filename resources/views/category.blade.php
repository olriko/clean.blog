@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="category">
            <h1>{{ $category->name }}</h1>
            <p>
                {{ $category->description }}
            </p>
            <div class="row">
                @each('components.article-card', $articles, 'article')
            </div>
            <div class="text-center">
                {{ $articles->links() }}
            </div>
        </div>
    </div>
@endsection

@section('title')
    <title> {{ Settings::get('app_name') }} - {{ $category->name }} </title>
@endsection