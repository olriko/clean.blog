@extends('layouts.app')

@section('content')
    <div id="settings" class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                @if (request()->session()->has('success'))
                    <div class="alert alert-success" role="alert">
                        {{ request()->session()->get('success') }}
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading text-center"><h3>Settings</h3></div>
                    <div class="panel-body">
                        @include('components.errors')
                        {{ Form::open(['url' => route('settings')]) }}
                        <div class="form-group">
                            {{ Form::label('name', 'Name') }}
                            {{ Form::text('name', auth()->user()->name , ['class' => 'form-control']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('email', 'Email') }}
                            {{ Form::text('email', auth()->user()->email, ['class' => 'form-control', 'disabled']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('password', 'Password') }}
                            {{ Form::password('password', ['class' => 'form-control']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('password_confirmation', 'Confirm Password') }}
                            {{ Form::password('password_confirmation', ['class' => 'form-control']) }}
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-primary">
                                Save
                            </button>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection