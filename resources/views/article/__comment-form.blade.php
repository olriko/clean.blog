@if (auth()->check() && auth()->user()->banned)
    <div class="text-danger">
        @lang('main.article.you-are-banned')
    </div>
@elseif(auth()->check())
    <div class="comment">
        <div class="photo">
            <div class="avatar" style="background-image: url('{{ auth()->user()->avatar }}')"></div>
        </div>
        <div class="comment-block">
            {{ Form::open(['url' => route('comment.store', ['id' => $article->id])]) }}
                {{ Form::textarea('comment', null, ['rows' => 3, 'cols' => 220, 'placeholder' => __('main.article.add-comment')]) }}
                {{ Form::button(__('main.article.submit'), ['type' => 'submit']) }}
            {{ Form::close() }}
        </div>
    </div>
@else
    <div class="comment guest">
        <div class="comment-block">
            <div class="text">
                @lang('main.article.login-required')
            </div>
        </div>
    </div>
@endif