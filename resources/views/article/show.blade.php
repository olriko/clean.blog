@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <article>
                    <div class="head">
                        <img src="{{ $article->image }}">
                    </div>
                    <div class="content">
                        <div class="date">
                            {{ $article->published_at->toFormattedDateString() }}
                        </div>
                        <h1>{{ $article->name }}</h1>
                        {!! html_entity_decode($article->content)  !!}
                    </div>
                    <div class="meta">
                        <div class="row">
                            <div class="col-md-6 text-center">
                                <a href="{{ 'https://www.facebook.com/sharer/sharer.php?u=' . url()->current() }}" class="social facebook"><i class="fa fa-facebook fa-fw"></i>Facebook</a>
                                <a href="{{ 'https://twitter.com/intent/tweet?text=' . $article->name . '&url=' . url()->current() }}" class="social twitter"><i class="fa fa-twitter fa-fw"></i>Twitter</a>
                            </div>
                            <div class="col-md-6">
                                @if ($article->user)
                                    <div class="author text-center">
                                        <div class="by"> @lang('main.article.wrote-by') </div>
                                        <div class="avatar">
                                            <img src="{{ $article->user->avatar }}">
                                        </div>
                                        <div class="name">
                                            {{ $article->user->name }}
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </article>
                <div class="comments">
                    @each('article.__comment', $article->comments , 'comment')
                    @include('article.__comment-form')
                </div>
            </div>
        </div>
    </div>
@endsection

@section('title')
    <title> {{ Settings::get('app_name') }} - {{ $article->name }} </title>
@endsection

@section('description')
    <meta name="description" content="{{ str_limit($article->description) }}">
@show

@section('og')
    <meta property="og:title" content="{{ Settings::get('app_name') . ' - ' . $article->name }}"/>
    <meta property="og:type" content="article"/>
    <meta property="og:image" content="{{ $article->image }}" />
    <meta property="article:published_time" content="{{ $article->published_at->toIso8601String() }}"/>
    @if ($article->user)
        <meta property="article:author" content="{{ $article->user->name }}"/>
    @endif
    <meta property="og:site_name" content="{{ Settings::get('app_name') }}"/>
    <meta property="og:url" content="{{ URL::current() }}"/>
@endsection
