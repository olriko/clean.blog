<div class="comment">
    <div class="photo">
        <div class="avatar"
             style="background-image: url('{{ $comment->user->avatar }}')"></div>
    </div>
    <div class="comment-block">
        <p class="comment-text">{{ $comment->content }}</p>
        <div class="bottom-comment">
            <div class="comment-date">{{ $comment->created_at->toFormattedDateString() }}</div>
            <ul class="comment-actions">
                <li class="complain">{{ $comment->user->name }}</li>
            </ul>
        </div>
    </div>
</div>
