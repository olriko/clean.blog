@extends('layouts._')

@section('_')
    <div id="admin">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    @include('partials.sidebar')
                </div>
                <div class="col-md-9">
                    @yield('admin')
                </div>
            </div>
        </div>
    </div>
@endsection