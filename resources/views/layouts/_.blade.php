<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @section('title')
        <title> {{ Settings::get('app_name') }} </title>
    @show


    @section('description')
        <meta name="description" content="{{ str_limit(Settings::get('footer_content')) }}">
    @show

    @section('og')
        <meta property="og:title" content="{{ Settings::get('app_name') }}"/>
        <meta property="og:type" content="website"/>
        <meta property="og:site_name" content="{{ Settings::get('app_name') }}"/>
        <meta property="og:url" content="{{ URL::current() }}"/>
    @show

    <link rel="canonical" href="{{ env('APP_URL') }}"/>

    <link rel="icon" type="image/png" href="{{ asset('storage/settings/favicon.png') }}"/>

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('trumbowyg/ui/trumbowyg.min.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
@if (Settings::get('ga'))
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', '{{ Settings::get('ga') }}', 'auto');
        ga('send', 'pageview');

    </script>
@endif

<div id="app">
    @include('partials.header')
    @yield('_')
</div>
@include('partials.footer')


<!-- Scripts -->
<script src="{{ mix('js/app.js') }}"></script>
<script src="{{ asset('trumbowyg/trumbowyg.min.js') }}"></script>
<script src="{{ asset('trumbowyg/plugins/upload/trumbowyg.upload.min.js') }}"></script>

</body>
</html>
