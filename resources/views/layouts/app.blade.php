@extends('layouts._')

@section('_')
    <div id="blog">
        @yield('content')
    </div>
@endsection