<div class="col-sm-4">
    <a href="{{ route('article.show', ['title' => str_slug($article->name), 'id' => $article->id]) }}">
        <div class="post-module">
            <div class="thumb">
                <div class="date">
                    <div class="day">{{ $article->published_at->day }}</div>
                    <div class="month">{{ $article->published_at->format('M') }}</div>
                </div>
                <img src="{{ $article->image }}"/>
            </div>
            <div class="post-content">
                @if ($article->published_at->isToday() ||  $article->published_at->isYesterday())
                    <div class="new">
                        @lang('main.article-card.new')
                    </div>
                @endif
                @if ($article->category)
                    <div class="category">{{ $article->category->name }}</div>
                @endif
                <h1 class="title">{{ $article->name }}</h1>
                <p class="description">{{ str_limit($article->description, 200) }}</p>
                <div class="post-meta">
                <span class="timestamp">
                     <i class="fa fa-clock-o fa-fw" aria-hidden="true"></i> {{ $article->published_at->diffForHumans() }}</span>
                    <span class="comments">
                    <i class="fa fa-comment-o fa-fw" aria-hidden="true"></i>{{ $article->comments()->count() }} @lang('main.article-card.comments')</span>
                </div>
            </div>
        </div>
    </a>
</div>