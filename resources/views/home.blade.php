@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @each('components.article-card', $articles, 'article')
        </div>
        <div class="text-center">
            {{ $articles->links() }}
        </div>
    </div>
@endsection
