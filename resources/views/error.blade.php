@extends('layouts.app')

@section('content')
    <div id="error">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center">
                <h1>{{ $exception->getStatusCode() }}</h1>
                <h3>An error occurred</h3>
            </div>
        </div>
    </div>
@endsection