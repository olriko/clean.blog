@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2> @lang('main.search')  {{ $query }}</h2>
            </div>
            @each('components.article-card', $articles, 'article')
        </div>
        <div class="text-center">
            {{ $articles->links() }}
        </div>
    </div>
@endsection
