<footer>
    <div class="container">
        <div class="row">
            @if($categories->count())
                <div class="col-md-2">
                    <h3> @lang('main.footer.category') </h3>
                    <ul>
                        @foreach($categories as $category)
                            <li>
                                <a href="{{ route('category.show', ['name' => str_slug($category->name), 'id' => $category->id]) }}"> {{ $category->name }} </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="col-md-2">
                @if (Settings::get('social_facebook') || Settings::get('social_youtube') || Settings::get('social_twitter') || Settings::get('social_instagram') || Settings::get('social_linkedIn'))
                    <h3> @lang('main.footer.social')</h3>
                    <ul>
                        @if(Settings::get('social_facebook'))
                            <li>
                                <a target="_blank" href="{{ Settings::get('social_facebook') }}"><i
                                            class="fa fa-facebook fa-fw"></i>Facebook</a>
                            </li>
                        @endif
                        @if(Settings::get('social_youtube'))
                            <li>
                                <a target="_blank" href="{{ Settings::get('social_facebook') }}"><i
                                            class="fa fa-youtube fa-fw"></i>Youtube</a>
                            </li>
                        @endif
                        @if(Settings::get('social_twitter'))
                            <li>
                                <a target="_blank" href="{{ Settings::get('social_twitter') }}"><i
                                            class="fa fa-twitter fa-fw"></i>Twitter</a>
                            </li>
                        @endif
                        @if(Settings::get('social_instagram'))
                            <li>
                                <a target="_blank" href="{{ Settings::get('social_instagram') }}"><i
                                            class="fa fa-instagram fa-fw"></i>Instagram</a>
                            </li>
                        @endif
                        @if(Settings::get('social_linkedIn'))
                            <li>
                                <a target="_blank" href="{{ Settings::get('social_linkedIn') }}"><i
                                            class="fa fa-linkedin fa-fw"></i>LinkedIn</a>
                            </li>
                        @endif
                    </ul>
                @endif

            </div>
            <div class="col-md-8">
                <h3> @lang('main.footer.about') </h3>
                <p>
                    {{ Settings::get('footer_content') }}
                </p>
            </div>
        </div>
    </div>
</footer>