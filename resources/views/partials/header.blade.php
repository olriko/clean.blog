<nav class="navbar navbar-default navbar-static-top navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ route('home') }}">
                @if (Storage::disk('public')->exists('settings/logo.png'))
                    <img class="logo" style="display: inline" src="{{ asset('storage/settings/logo.png') }}"> @endif
                    {{ Settings::get('app_name') }}
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                &nbsp;@foreach($categories as $category)
                    <li>
                        <a href="{{ route('category.show', ['name' => str_slug($category->name), 'id' => $category->id]) }}">
                            {{ $category->navbar_name }}
                        </a>
                    </li>
                @endforeach
            </ul>
            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @if (Auth::guest())
                    <li><a href="{{ route('login') }}"> @lang('main.header.login') / @lang('main.header.register') </a></li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li>
                            @if (auth()->user()->admin)
                                <li>
                                    <a href="{{ route('admin.home') }}">Admin</a>
                                </li>
                            @endif
                            <li>
                                <a href="{{ route('settings') }}">@lang('main.header.settings')</a>
                            </li>
                            <li>

                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    @lang('main.header.logout')
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
            <form method="get" action="{{ route('search') }}" class="navbar-form navbar-right">
                {{ csrf_field() }}
                <div class="form-group">
                    <input type="text" name="query" class="form-control" placeholder="@lang('main.header.search')">
                </div>
            </form>
        </div>
    </div>
</nav>