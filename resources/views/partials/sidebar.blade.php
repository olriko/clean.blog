<div class="list-group">
    <a href="{{ route('admin.home') }}" class="list-group-item @active('admin/home/?.*') ">Home</a>
    <a href="{{ route('admin.settings.index') }}" class="list-group-item @active('admin/settings/?.*') ">Settings</a>
</div>

<div class="list-group">
    <a href="{{ route('admin.user.index') }}" class="list-group-item @active('admin/user/?.*') ">Users</a>
    <a href="{{ route('admin.article.index') }}" class="list-group-item @active('admin/article/?.*') ">Articles</a>
    <a href="{{ route('admin.category.index') }}" class="list-group-item @active('admin/category/?.*') ">Categories</a>
</div>