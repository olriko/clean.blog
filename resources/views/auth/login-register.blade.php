@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div id="sign">
                    @if (Settings::get('auth_google_id'))
                        <a href="{{ route('login.google.redirect') }}" class="google">
                            <i class="fa fa-google-plus"></i> Login with Google
                        </a>
                        <hr>
                    @endif
                    <div class="row">
                        <div class="col-md-6">
                            @include('auth.__login-form')
                        </div>
                        <div class="col-md-6">
                            @include('auth.__register-form')
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
