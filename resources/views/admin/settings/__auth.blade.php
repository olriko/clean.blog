<div class="panel panel-default">
    <div class="panel-heading">Authentification</div>
    <div class="panel-body">
        <div class="form-group">
            {{ Form::label('auth_google_id', 'Google', ['class' => 'col-sm-3']) }}
            <div class="col-sm-5">
                {{ Form::text('auth_google_id', Settings::get('auth_google_id'), ['class' => 'form-control', 'placeholder' => 'Client ID']) }}
            </div>
            <div class="col-sm-4">
                {{ Form::text('auth_google_secret', Settings::get('auth_google_secret'), ['class' => 'form-control', 'placeholder' => 'Secret ID']) }}
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
                {{ Form::submit('Save', ['class' => 'btn-primary btn-block']) }}
            </div>
        </div>
    </div>
</div>