@extends('layouts.admin')

@section('admin')
    <div class="row">
        <div class="col-md-12">
            <h1>Settings</h1>
            @include('components.errors')
            {{ Form::open(['url' => route('admin.settings.store'), 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data' ]) }}
                @include('admin.settings.__global')
                @include('admin.settings.__auth')
                @include('admin.settings.__social')
            {{ Form::close() }}

        </div>
    </div>
@endsection
