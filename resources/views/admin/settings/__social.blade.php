<div class="panel panel-default">
    <div class="panel-heading">Social</div>
    <div class="panel-body">
        <div class="form-group">
            {{ Form::label('social_facebook', 'Facebook', ['class' => 'col-sm-3']) }}
            <div class="col-sm-9">
                {{ Form::text('social_facebook', Settings::get('social_facebook'), ['class' => 'form-control', 'placeholder' => 'ex: https://www.facebook.com/cocacola']) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('social_twitter', 'Twitter', ['class' => 'col-sm-3']) }}
            <div class="col-sm-9">
                {{ Form::text('social_twitter', Settings::get('social_twitter'), ['class' => 'form-control', 'placeholder' => 'ex: https://twitter.com/CocaCola']) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('social_youtube', 'Youtube', ['class' => 'col-sm-3']) }}
            <div class="col-sm-9">
                {{ Form::text('social_youtube', Settings::get('social_youtube'), ['class' => 'form-control', 'placeholder' => 'ex: https://www.youtube.com/user/cocacola']) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('social_instagram', 'Instagram', ['class' => 'col-sm-3']) }}
            <div class="col-sm-9">
                {{ Form::text('social_instagram', Settings::get('social_instagram'), ['class' => 'form-control', 'placeholder' => 'ex: https://www.instagram.com/cocacola']) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('social_linkedIn', 'LinkedIn', ['class' => 'col-sm-3']) }}
            <div class="col-sm-9">
                {{ Form::text('social_linkedIn', Settings::get('social_linkedIn'), ['class' => 'form-control', 'placeholder' => 'ex: https://www.linkedin.com/company-beta/1694/?pathWildcard=1694']) }}
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
                {{ Form::submit('Save', ['class' => 'btn-primary btn-block']) }}
            </div>
        </div>
    </div>
</div>