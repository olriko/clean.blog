<div class="panel panel-default">
    <div class="panel-heading">Global</div>
    <div class="panel-body">
        <div class="form-group">
            {{ Form::label('app_name', 'App name', ['class' => 'col-sm-3']) }}
            <div class="col-sm-9">
                {{ Form::text('app_name', Settings::get('app_name'), ['class' => 'form-control']) }}
                <span class="help-block">Name of your blog/website</span>
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('footer_content', 'About Footer', ['class' => 'col-sm-3']) }}
            <div class="col-sm-9">
                {{ Form::textarea('footer_content', Settings::get('footer_content'), ['class' => 'form-control', 'rows' => 3]) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('favicon', 'Favicon', ['class' => 'col-sm-3']) }}
            <div class="col-sm-9">
                {{ Form::file('favicon') }}
                <span class="help-block">Format: 32px x 32px in '.png'. Current favicon: <img width="16" style="display: inline" src="{{ asset('storage/settings/favicon.png') }}"></span>
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('logo', 'Logo', ['class' => 'col-sm-3']) }}
            <div class="col-sm-9">
                {{ Form::file('logo') }}
                <span class="help-block">Max height 70px in '.png'. @if (Storage::disk('public')->exists('settings/logo.png')) Current logo: <img width="50" style="display: inline" src="{{ asset('storage/settings/logo.png') }}"> @endif </span>
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('ga', 'Google Analytic ID', ['class' => 'col-sm-3']) }}
            <div class="col-sm-9">
                {{ Form::text('ga', Settings::get('ga'), ['class' => 'form-control', 'placeholder' => 'ex: UA-85532742-1']) }}
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
                {{ Form::submit('Save', ['class' => 'btn-primary btn-block']) }}
            </div>
        </div>
    </div>
</div>