@extends('admin.category._layout')

@section('category')
    <div class="panel panel-default">
        <div class="panel-body">
            @include('admin.category.form', ['model' => $category, 'method' => 'POST', 'url' => route('admin.category.store')])
        </div>
    </div>
@endsection

@section('type', 'Create')