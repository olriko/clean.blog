@extends('admin.category._layout')

@section('category')
    <div class="panel panel-default">
        <div class="panel-body">
            <table class="table table-hover">
                <thead>
                <tr>
                    <td>#</td>
                    <td>Name</td>
                    <td><a href="{{ route('admin.category.create') }}" class="btn btn-success btn-xs pull-right">
                            Add category <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                        </a>
                    </td>
                </tr>
                </thead>
                <tbody>
                @foreach($categories as $category)
                    <tr>
                        <td>{{ $category->id }}</td>
                        <td>{{ $category->name }}</td>
                        <td>
                            <div class="pull-right">
                                <a href="{{ route('admin.category.edit', ['category' => $category]) }}"
                                   class="btn btn-default btn-xs">
                                    <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                </a>
                                <button type="button" class="btn btn-xs btn-default" data-toggle="modal"
                                        data-target="#rm-{{$category->id}}"><span class="glyphicon glyphicon-remove"
                                                                                  aria-hidden="true"></span></button>
                                <div id="rm-{{ $category->id }}" class="modal fade bs-example-modal-sm" tabindex="-1"
                                     role="dialog" aria-labelledby="mySmallModalLabel">
                                    <div class="modal-dialog modal-sm" role="document">
                                        <div class="modal-content">
                                            {{ Form::open(['url' => route('admin.category.destroy', ['category' => $category]), 'method' => 'DELETE', 'class' => '']) }}
                                            {{ Form::button('Delete', ['type' => 'submit', 'class' => 'btn btn-block btn-danger']) }}
                                            {{ Form::close() }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $categories->links() }}
        </div>
    </div>
@endsection