@include('components.errors')
{{ Form::model($model,['url' => $url, 'method' => $method]) }}

<div class="form-group">
    {{ Form::label('name', 'Name') }}
    {{ Form::text('name', null, ['class' => 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::label('navbar_name', 'Navbar name') }}
    {{ Form::text('navbar_name', null, ['class' => 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::label('description', 'Description') }}
    {{ Form::textarea('description', null, ['class' => 'form-control', 'rows' => 2]) }}
</div>
<div class="form-group">
    {{ Form::checkbox('navbar_display', null) }}
    {{ Form::label('navbar_display', 'Display on navbar ?') }}
</div>
<div class="form-group">
    {{ Form::label('order', 'Navbar order') }}
    {{ Form::number('order', 0, ['class' => 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::submit('Save',['class' => 'btn-primary btn-block']) }}
</div>
{{ Form::close() }}