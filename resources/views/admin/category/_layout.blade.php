@extends('layouts.admin')

@section('admin')
    <div class="row">
        <div class="col-md-9">
            <h3>Category <small> @yield('type') </small></h3>
        </div>
    </div>
    @yield('category')
@endsection