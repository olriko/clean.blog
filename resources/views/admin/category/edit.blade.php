@extends('admin.category._layout')

@section('category')
    <div class="panel panel-default">
        <div class="panel-body">
            @include('admin.category.form', ['model' => $category, 'method' => 'PUT', 'url' => route('admin.category.update', ['category' => $category])])
        </div>
    </div>
@endsection

@section('type', 'Edit')