@extends('admin.article._layout')

@section('article')
    <div class="panel panel-default">
        <div class="panel-body">
            @include('admin.article.form', ['model' => $article, 'method' => 'PUT', 'url' => route('admin.article.update', ['article' => $article])])
        </div>
    </div>
@endsection

@section('type', 'Edit')