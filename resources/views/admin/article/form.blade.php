@include('components.errors')
{{ Form::model($model,['url' => $url, 'method' => $method, 'enctype' => 'multipart/form-data']) }}

<div class="form-group">
    {{ Form::label('name', 'Title (required)') }}
    {{ Form::text('name', null, ['class' => 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::label('description', 'Description') }}
    {{ Form::textarea('description', null, ['class' => 'form-control', 'rows' => 4]) }}
</div>
<div class="form-group">
    {{ Form::label('content', 'Content') }}
    <div id="content">
        {{ $model->content }}
    </div>
</div>
<div class="form-group">
    <div class="row">
        <div class="col-xs-4">
            {{ Form::label('category_id', 'Category') }}
            {{ Form::select('category_id', App\Models\Category::get()->pluck('name' ,'id')->all() , null, ['placeholder' => 'Pick a category (or not)', 'class' => 'form-control']) }}
        </div>
        <div class="col-xs-4">
            {{ Form::label('user_id', 'Author') }}
            {{ Form::select('user_id',  App\Models\User::where('admin', true)->get()->pluck('name' ,'id')->all() , auth()->user()->id, ['class' => 'form-control']) }}
        </div>
        <div class="col-xs-4">
            {{ Form::label('image', 'Image') }}
            {{ Form::file('image') }}
            <div class="help-block">Format: 750 x 300</div>
            @if($model->image)
                <button class="btn btn-primary btn-xs btn-block" type="button" data-toggle="collapse"
                        data-target="#collapseImage" aria-expanded="false" aria-controls="collapseExample">
                    Display image
                </button>
                <div class="collapse" id="collapseImage">
                    <div class="well">
                        <img width="100%" src="{{ $model->image }}">
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>

<div class="form-group">
    <div class="row">
        <div class="col-xs-8">
            {{ Form::button('Save', ['class' => 'btn-primary btn-block', 'name' => 'submit',  'type' => 'submit', 'value' => 'save']) }}
        </div>
        <div class="col-xs-4">
            @if ($model->published)
                {{ Form::button('Unpublish', ['class' => 'btn-danger btn-block', 'name' => 'submit', 'type' => 'submit', 'value' => 'unpublish']) }}
            @else
                {{ Form::button('Publish', ['class' => 'btn-warning btn-block', 'name' => 'submit',  'type' => 'submit', 'value' => 'publish']) }}
            @endif
        </div>

    </div>
</div>
{{ Form::close() }}