@extends('admin.article._layout')

@section('article')
    <div class="panel panel-default">
        <div class="panel-body">
            @include('admin.article.form', ['model' => $article, 'method' => 'POST', 'url' => route('admin.article.store')])
        </div>
    </div>
@endsection

@section('type', 'Create')