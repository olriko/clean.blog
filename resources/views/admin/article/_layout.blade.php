@extends('layouts.admin')

@section('admin')
    <div class="row">
        <div class="col-md-9">
            <h3>Article <small> @yield('type') </small></h3>
        </div>
        <div class="col-md-3">
            <a href="{{ route('admin.article.create') }}" type="button" class="btn btn-primary">Create new article ?</a>
        </div>
    </div>
    @yield('article')
@endsection