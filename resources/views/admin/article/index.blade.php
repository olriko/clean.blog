@extends('admin.article._layout')

@section('article')
    <div class="panel panel-default">
        <div class="panel-body">
            <table class="table table-hover">
                <thead>
                <tr>
                    <td>#</td>
                    <td>Name</td>
                    <td>Author</td>
                    <td class="text-center">Views</td>
                    <td class="text-center">Published</td>
                    <td class="text-center">Actions</td>
                </tr>
                </thead>
                <tbody>
                @foreach($articles as $article)
                    <tr>
                        <td>{{ $article->id }}</td>
                        <td>{{ $article->name }}</td>
                        <td>{{ $article->user->name }}</td>
                        <td class="text-center">{{ $article->views }}</td>
                        <td class="text-center">
                            @if($article->published)
                                <span class="glyphicon glyphicon-ok"></span>
                            @else
                                <span class="glyphicon glyphicon-remove"></span>
                            @endif
                        </td>
                        <td class="text-center">
                            <a href="{{ route('admin.article.edit', ['article' => $article]) }}"
                               class="btn btn-default btn-xs">
                                <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                            </a>
                            <button type="button" class="btn btn-xs btn-default" data-toggle="modal" data-target="#rm-{{$article->id}}"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                            <div id="rm-{{ $article->id }}" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
                                <div class="modal-dialog modal-sm" role="document">
                                    <div class="modal-content">
                                        {{ Form::open(['url' => route('admin.article.destroy', ['article' => $article->id]), 'method' => 'delete']) }}
                                            {{ Form::button('Delete', ['type' => 'submit', 'class' => 'btn btn-block btn-danger']) }}
                                        {{ Form::close() }}
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $articles->links() }}
        </div>
    </div>
@endsection