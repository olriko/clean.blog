@extends('layouts.admin')

@section('admin')
    <div id="home" class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body text-center">
                    Welcome
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading text-center">
                    Total Articles
                </div>
                <div class="panel-body text-center">
                    <h1>{{ $articles->count() }}</h1>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading text-center">
                    Total Users
                </div>
                <div class="panel-body text-center">
                    <h1>{{ $users->count() }}</h1>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading text-center">
                    Total Views
                </div>
                <div class="panel-body text-center">
                    <h1>{{ $articles->sum('views') }}</h1>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading text-center">
                    Total Comments
                </div>
                <div class="panel-body text-center">
                    <h1>{{ $comments->count() }}</h1>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading text-center">
                    Top 3 Articles by Comments
                </div>
                <div class="panel-body text-center">
                    @foreach($articles_by_comments as $article)
                        {{ $article->name }}  {{ $article->comments->count() }} <br>
                    @endforeach

                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading text-center">
                    Top 3 Articles by Views
                </div>
                <div class="panel-body text-center">
                    @foreach($articles_by_views as $article)
                        {{ $article->name }}  {{ $article->views }} <br>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
@endsection