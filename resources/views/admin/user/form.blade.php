@include('components.errors')
{{ Form::model($model,['url' => $url, 'method' => $method]) }}
<div class="form-group">
    {{ Form::label('email', 'Email') }}
    {{ Form::text('email', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('name', 'Name') }}
    {{ Form::text('name', null, ['class' => 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::checkbox('admin', null) }}
    {{ Form::label('admin', 'Admin') }}
</div>
<div class="form-group">
    {{ Form::checkbox('banned', null) }}
    {{ Form::label('banned', 'Banned') }}
</div>
<div class="form-group">
    {{ Form::submit('Save',['class' => 'btn-primary btn-block']) }}
</div>
{{ Form::close() }}