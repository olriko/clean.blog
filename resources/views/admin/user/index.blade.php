@extends('admin.user._layout')

@section('user')
    <div class="panel panel-default">
        <div class="panel-body">
            <table class="table table-hover">
                <thead>
                <tr>
                    <td>#</td>
                    <td></td>
                    <td>Name</td>
                    <td>Email</td>
                    <td class="text-center">Actions</td>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>
                            @if($user->avatar)
                                <img width="32" src="{{ $user->avatar }}" alt="{{ $user->name  }}" class="img-circle">
                            @endif
                        </td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td class="text-center">
                            <a href="{{ route('admin.user.edit', ['user' => $user]) }}"
                               class="btn btn-default btn-xs">
                                <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $users->links() }}
        </div>
    </div>
@endsection