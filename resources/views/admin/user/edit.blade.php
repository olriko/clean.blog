@extends('admin.user._layout')

@section('user')
    <div class="panel panel-default">
        <div class="panel-body">
            @include('admin.user.form', ['model' => $user, 'method' => 'PUT', 'url' => route('admin.user.update', ['user' => $user])])
        </div>
    </div>
@endsection

@section('type', 'Edit')