@extends('layouts.admin')

@section('admin')
    <h3>User <small> @yield('type') </small></h3>
    @yield('user')
@endsection