<?php

return [

    //Header
    'header' => [
        'search' => 'Search',
        'settings' => 'Settings',
        'logout' => 'Logout',
        'admin' => 'Amdin',
        'login' => 'Login',
        'register' => 'Register'
    ],

    //Search

    'search' => 'Search :',

    //Article
    'article' => [
        'add-comment' => 'Add comment ...',
        'submit' => 'Submit',
        'wrote-by' => 'Wrote by',
        'you-are-banned' => ' You are banned.',
        'login-required' => 'You must be logged to leave a comment.',
    ],

    // Article card

    'article-card' => [
        'new' => 'New',
        'comments' => 'Comments',
    ],

    // Footer
    'footer' => [
        'about' => 'About',
        'category' => 'Category',
        'social' => 'Social'
    ]
];